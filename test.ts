
import { merge } from './merge';


describe('merge', () => {
  it('two sorted arrays 1', () => {
    const result = merge([4, 1, 5], [9, 8, 6]);
    expect(result).toEqual([1, 4, 5, 6, 8, 9]);
  });

  it('two sorted arrays 2', () => {
    const result = merge([9,2,4], [1,0,10]);
    expect(result).toEqual([0,1,2,4,9,10]);
  });

  it('empty arrays', () => {
    const result = merge([], []);
    expect(result).toEqual([]);
  });

  it('one array is empty', () => {
    const result = merge([1, 3, 5], []);
    expect(result).toEqual([1, 3, 5]);
  });

});
